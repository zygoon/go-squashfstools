// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashlzo contains LZO compression configuration for mksquashfs.
package squashlzo

import (
	"fmt"
	"strconv"
)

// Config describes configuration of the LZO compression for mksquashfs(1).
type Config struct {
	Algorithm Algorithm

	// CompressionLevel controls compression level.
	// The value must be in range 1 .. 9
	// The value is only used by the algorithm Lzo1x_999.
	CompressionLevel uint8
}

// CompressionName returns "lzo".
func (*Config) CompressionName() string {
	return "lzo"
}

// MksquashfsOptions returns mksquashfs compression tuning options.
func (conf *Config) MksquashfsOptions() (opts []string) {
	if conf.Algorithm == Lzo1x999 {
		if val := conf.CompressionLevel; val != 0 {
			opts = append(opts, "-X"+"compression-level", strconv.FormatUint(uint64(val), 10))
		}
	} else {
		opts = append(opts, "-X"+"algorithm", conf.Algorithm.String())
	}

	return opts
}

// Algorithm identifies a compression algorithm specific to LZO.
type Algorithm uint8

const (
	// Lzo1x999 is an LZO compression algorithm.
	Lzo1x999 Algorithm = iota
	// Lzo1x1 is an LZO compression algorithm.
	Lzo1x1
	// Lzo1x1type11 is an LZO compression algorithm.
	Lzo1x1type11
	// Lzo1x1type12 is an LZO compression algorithm.
	Lzo1x1type12
	// Lzo1x1type15 is an LZO compression algorithm.
	Lzo1x1type15
)

// String returns the name of the LZO compression algorithm.
func (a Algorithm) String() string {
	switch a {
	case Lzo1x1:
		return "lzo1x_1"
	case Lzo1x1type11:
		return "lzo1x_1_11"
	case Lzo1x1type12:
		return "lzo1x_1_12"
	case Lzo1x1type15:
		return "lzo1x_1_15"
	case Lzo1x999:
		return "lzo1x_999"
	default:
		panic(fmt.Sprintf("invalid LZO algorithm: %d", int(a)))
	}
}
