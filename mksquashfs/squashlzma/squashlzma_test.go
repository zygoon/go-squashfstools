// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashlzma_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlzma"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashlzma.Config{}
	testutil.Equal(t, cfg.CompressionName(), "lzma")
}

func TestConfig_MksquashfsOptions(t *testing.T) {
	cfg := squashlzma.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}
