// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashxz_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashxz"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashxz.Config{}
	testutil.Equal(t, cfg.CompressionName(), "xz")
}

func TestConfig_MksquashfsOptions(t *testing.T) {
	cfg := squashxz.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}

func TestConfig_MksquashfsOptionsWithDictionarySize(t *testing.T) {
	t.Run("percentage", func(t *testing.T) {
		cfg := squashxz.Config{DictionarySize: "50%"}
		testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "dict-size", "50%"})
	})

	t.Run("absolute", func(t *testing.T) {
		cfg := squashxz.Config{DictionarySize: "16K"}
		testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "dict-size", "16K"})
	})
}

func TestConfig_MksquashfsOptionsWithCompressionFilters(t *testing.T) {
	cfg := squashxz.Config{CompressionFilters: []string{"x86", "arm"}}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "bcj", "x86,arm"})
}
