// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashzstd contains zstd compression configuration for mksquashfs.
package squashzstd

import (
	"strconv"
)

// Config describes configuration of the zstd compression for mksquashfs(1).
type Config struct {
	// CompressionLevel controls compression level.
	// The value must be in range 1 .. 22
	CompressionLevel uint8
}

// CompressionName returns "zstd".
func (conf *Config) CompressionName() string {
	return "zstd"
}

// MksquashfsOptions returns mksquashfs compression tuning options.
func (conf *Config) MksquashfsOptions() (opts []string) {
	if val := conf.CompressionLevel; val != 0 {
		opts = append(opts, "-X"+"compression-level", strconv.FormatUint(uint64(val), 10))
	}

	return opts
}
