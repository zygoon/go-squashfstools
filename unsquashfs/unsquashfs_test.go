// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package unsquashfs_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/unsquashfs"
)

func TestCommand_ExtractWithoutOptions(t *testing.T) {
	cmd := unsquashfs.Command("input.img", "output-dir", nil, "a-file", "a-dir")
	testutil.SlicesEqual(t, cmd.Args, []string{
		"unsquashfs", "-no-progress", "-no-xattrs", "-dest", "output-dir", "input.img", "a-file", "a-dir",
	})
}

func TestExtractWithXattrs(t *testing.T) {
	// There's an affirmative option for xattrs, unlike most of the other options.
	cmd := unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{Xattrs: true}, "a-file")
	testutil.WithOption(t, cmd, "-xattrs")
	testutil.WithoutOption(t, cmd, "-no-xattrs")

	cmd = unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{Xattrs: false}, "a-file")
	testutil.WithoutOption(t, cmd, "-xattrs")
	testutil.WithOption(t, cmd, "-no-xattrs")
}

func TestExtractWithNumProcessors(t *testing.T) {
	cmd := unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{NumProcessors: 2}, "a-file")
	testutil.WithOptionValue(t, cmd, "-processors", "2")

	cmd = unsquashfs.Command("input.img", "dir", &unsquashfs.Options{NumProcessors: 0}, "output-file", "output-dir")
	testutil.WithoutOption(t, cmd, "-processors")
}

func TestExtractWithDataQueueSize(t *testing.T) {
	cmd := unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{DataQueueSizeMB: 16}, "a-file")
	testutil.WithOptionValue(t, cmd, "-data-queue", "16")

	cmd = unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{DataQueueSizeMB: 0}, "a-file")
	testutil.WithoutOption(t, cmd, "-data-queue")
}

func TestExtractWithFragmentQueueSize(t *testing.T) {
	cmd := unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{FragmentQueueSizeMB: 16}, "a-file")
	testutil.WithOptionValue(t, cmd, "-frag-queue", "16")

	cmd = unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{FragmentQueueSizeMB: 0}, "a-file")
	testutil.WithoutOption(t, cmd, "-frag-queue")
}

func TestExtractWithForce(t *testing.T) {
	cmd := unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{Force: true}, "a-file")
	testutil.WithOption(t, cmd, "-force")

	cmd = unsquashfs.Command("input.img", "output-dir", &unsquashfs.Options{Force: false}, "a-file")
	testutil.WithoutOption(t, cmd, "-force")
}
