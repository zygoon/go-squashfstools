// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import (
	"fmt"
	"os/exec"
	"slices"
	"testing"
)

// RequiresProgram skips execution of the test if the given program is absent.
func RequiresProgram(t *testing.T, progs ...string) {
	t.Helper()

	if len(progs) == 0 {
		t.Fatal("At least one program name is required")
	}

	for _, prog := range progs {
		if _, err := exec.LookPath(prog); err != nil {
			t.Skipf("Required program not available: %v", err)
		}
	}
}

// WithOption checks that the given command has a given option.
func WithOption(t *testing.T, cmd *exec.Cmd, option string) {
	t.Helper()

	_, err := optionIndex(cmd, option)
	if err != nil {
		showArguments(t, cmd)
		t.Fatal(err)
	}
}

// WithoutOption checks that the given command does not have the given option.
func WithoutOption(t *testing.T, cmd *exec.Cmd, option string) {
	t.Helper()

	_, err := optionIndex(cmd, option)
	if err == nil {
		showArguments(t, cmd)
		t.Fatalf("Option %v unexpectedly present", option)
	}
}

// WithOptionValue checks that the given command has a given option and matching value.
func WithOptionValue(t *testing.T, cmd *exec.Cmd, option, value string) {
	t.Helper()

	actualValue, err := optionValue(cmd, option)
	if err != nil {
		showArguments(t, cmd)
		t.Fatal(err)
	}

	if value != actualValue {
		showArguments(t, cmd)
		t.Fatalf("Option %v has unexpected value: %v != %v", option, value, actualValue)
	}
}

func showArguments(t *testing.T, cmd *exec.Cmd) {
	t.Helper()

	t.Logf("Command arguments: %q", cmd.Args)
}

func optionIndex(cmd *exec.Cmd, opt string) (int, error) {
	idx := slices.Index(cmd.Args, opt)
	if idx == -1 {
		return -1, fmt.Errorf("option not found: %v", opt)
	}

	return idx, nil
}

func optionValue(cmd *exec.Cmd, opt string) (string, error) {
	idx, err := optionIndex(cmd, opt)
	if err != nil {
		return "", err
	}

	if idx+1 >= len(cmd.Args) {
		return "", fmt.Errorf("option without value: %v", opt)
	}

	return cmd.Args[idx+1], nil
}
