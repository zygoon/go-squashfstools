// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashlz4 contains LZ4 compression configuration for mksquashfs.
package squashlz4

// Config describes configuration of the LZ4 compression for mksquashfs(1).
type Config struct {
	// HighCompression controls the LZ4 high compression mode.
	HighCompression bool
}

// CompressionName returns "lz4".
func (*Config) CompressionName() string {
	return "lz4"
}

// MksquashfsOptions returns mksquashfs compression tuning options.
func (conf *Config) MksquashfsOptions() (opts []string) {
	if conf.HighCompression {
		return []string{"-Xhc"}
	}

	return nil
}
