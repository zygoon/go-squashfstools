// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashlz4_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlz4"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashlz4.Config{}
	testutil.Equal(t, cfg.CompressionName(), "lz4")
}

func TestConfig_MksquashfsOptionsDefaults(t *testing.T) {
	cfg := squashlz4.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}

func TestConfig_MksquashfsOptionsWithHighCompression(t *testing.T) {
	cfg := squashlz4.Config{HighCompression: true}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "hc"})
}
