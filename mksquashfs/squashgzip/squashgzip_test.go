// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashgzip_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashgzip"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashgzip.Config{}
	testutil.Equal(t, cfg.CompressionName(), "gzip")
}

func TestConfig_MksquashfsOptionsDefaults(t *testing.T) {
	cfg := squashgzip.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}

func TestConfig_MksquashfsOptionsWithCompressionLevel(t *testing.T) {
	cfg := squashgzip.Config{CompressionLevel: 9}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "compression-level", "9"})
}

func TestConfig_MksquashfsOptionsWithWindowSize(t *testing.T) {
	cfg := squashgzip.Config{WindowSize: 15}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "window-size", "15"})
}

func TestConfig_MksquashfsOptionsWithStrategy(t *testing.T) {
	cfg := squashgzip.Config{
		Strategy: []squashgzip.Strategy{
			squashgzip.Filtered,
			squashgzip.RunLengthEncoded,
		},
	}

	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "strategy", "filtered,run_length_encoded"})
}

func TestStrategy_String(t *testing.T) {
	testutil.Equal(t, squashgzip.Default.String(), "default")
	testutil.Equal(t, squashgzip.Filtered.String(), "filtered")
	testutil.Equal(t, squashgzip.HuffmanOnly.String(), "huffman_only")
	testutil.Equal(t, squashgzip.RunLengthEncoded.String(), "run_length_encoded")
	testutil.Equal(t, squashgzip.Fixed.String(), "fixed")
	testutil.PanicMatches(t, func() { _ = squashgzip.Strategy(42).String() }, "invalid gzip strategy: 42")
}
