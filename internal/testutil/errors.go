// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import (
	"fmt"
	"regexp"
	"testing"
)

// Ok ensures that an error is nil.
func Ok(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatal(err)
	}
}

// ErrorMatches ensures that error message of the given error matches the given regular expression.
func ErrorMatches(t *testing.T, e error, pattern string) {
	t.Helper()

	if e == nil {
		t.Fatal("Error is unexpectedly nil")
	}

	errorMsg := e.Error()

	if matched, err := regexp.MatchString(pattern, errorMsg); err != nil {
		t.Fatal(err)
	} else if !matched {
		t.Logf("Error type: %T", e)
		t.Logf("Error message: %v", errorMsg)
		t.Fatalf("Error message does not match pattern: %v", pattern)
	}
}

// PanicMatches ensures that function panics with a message that matches the given regular expression.
func PanicMatches(t *testing.T, fn func(), pattern string) {
	t.Helper()

	var panicValue any

	func() {
		defer func() {
			panicValue = recover()
		}()

		fn()
	}()

	if panicValue == nil {
		t.Fatal("Function did not panic")
	}

	panicMsg := fmt.Sprintf("%v", panicValue)

	if matched, err := regexp.MatchString(pattern, panicMsg); err != nil {
		t.Fatal(err)
	} else if !matched {
		t.Logf("Panic type: %T", panicValue)
		t.Logf("Panic message: %v", panicMsg)
		t.Fatalf("Panic message does not match pattern: %v", pattern)
	}
}
