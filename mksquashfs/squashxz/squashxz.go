// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashxz contains xz compression configuration for mksquashfs.
package squashxz

import (
	"strings"
)

// Config describes configuration of the LZO compression for mksquashfs(1).
type Config struct {
	// CompressionFilters defines the list of xz-specific compression filters.
	// Consult mksquashfs(1) for details.
	// Available filters: x86, arm, armthumb, powerpc, sparc, ia64
	CompressionFilters []string

	// DictionarySize defines the size of the XZ dictionary to use. The size
	// can be defined as a percentage of the block size or as an absolute value.
	// There are constraints for allowed values. Consult mksquashfs(1) for
	// details.
	DictionarySize string
}

// CompressionName returns "xz".
func (*Config) CompressionName() string {
	return "xz"
}

// MksquashfsOptions returns mksquashfs compression tuning options.
func (conf *Config) MksquashfsOptions() (opts []string) {
	if len(conf.CompressionFilters) != 0 {
		opts = append(opts, "-X"+"bcj", strings.Join(conf.CompressionFilters, ","))
	}

	if conf.DictionarySize != "" {
		opts = append(opts, "-X"+"dict-size", conf.DictionarySize)
	}

	return opts
}
