// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package unsquashfs provides a Go interface to the unsquashfs(1) program.
package unsquashfs

import (
	"errors"
	"os/exec"
	"strconv"

	"gitlab.com/zygoon/go-squashfstools"
)

// Options contains options for extracting files from images.
type Options struct {
	// Xattrs controls extraction of extended attributes.
	Xattrs bool
	// Extract even if files exists.
	Force bool

	// NumProcessors constrains concurrency factor.
	NumProcessors uint
	// DataQueueSizeMB defines the size of the data queue in megabytes.
	DataQueueSizeMB uint
	// FragmentQueueSizeMB defines the size of the fragment queue in megabytes.
	FragmentQueueSizeMB uint
}

const (
	cmdUnsquashfs = "unsquashfs"

	flagDataQueue      = "-data-queue"
	flagDestinationDir = "-dest"
	flagForce          = "-force"
	flagFragmentQueue  = "-frag-queue"
	flagNoProgress     = "-no-progress"
	flagNoXattrs       = "-no-xattrs"
	flagProcessors     = "-processors"
	flagQuiet          = "-quiet"
	flagXattrs         = "-xattrs"
)

// Command returns a configured unsquashfs command.
//
// Selected files are extracted with full path, relative to toDir.
func Command(fromImg, toDir string, opts *Options, files ...string) *exec.Cmd {
	if opts == nil {
		opts = &Options{}
	}

	// Do not show progress while extracting.
	args := []string{flagNoProgress}

	if !opts.Xattrs {
		args = append(args, flagNoXattrs)
	} else {
		args = append(args, flagXattrs)
	}

	if opts.Force {
		args = append(args, flagForce)
	}

	if opts.NumProcessors != 0 {
		args = append(args, flagProcessors, strconv.FormatUint(uint64(opts.NumProcessors), 10))
	}

	if opts.DataQueueSizeMB != 0 {
		args = append(args, flagDataQueue, strconv.FormatUint(uint64(opts.DataQueueSizeMB), 10))
	}

	if opts.FragmentQueueSizeMB != 0 {
		args = append(args, flagFragmentQueue, strconv.FormatUint(uint64(opts.FragmentQueueSizeMB), 10))
	}

	// Extract to the given directory.
	args = append(args, flagDestinationDir, toDir)

	// Extract from the given image.
	args = append(args, fromImg)

	// Extract those files
	args = append(args, files...)

	return exec.Command(cmdUnsquashfs, args...)
}

// Run executes unsquashfs(1) with the prescribed configuration.
func Run(fromImg, toDir string, opts *Options, files ...string) error {
	cmd := Command(fromImg, toDir, opts, files...)
	_, err := cmd.Output()

	var ee *exec.ExitError

	if errors.As(err, &ee) {
		return squashfstools.NewCommandError(cmd, ee)
	}

	return err
}
