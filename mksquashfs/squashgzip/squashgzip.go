// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashgzip contains gzip compression configuration for mksquashfs.
package squashgzip

import (
	"fmt"
	"strconv"
	"strings"
)

// Config describes configuration of the gzip compression for mksquashfs(1).
type Config struct {
	// CompressionLevel controls compression level.
	// The value must be in range 1 .. 9
	CompressionLevel uint8

	// WindowSize controls compression window size.
	// The value must be in rage 8 .. 15
	WindowSize uint8

	// Strategy instructs mksquashfs to find the best compression strategy out
	// of the provided set.
	Strategy []Strategy
}

// CompressionName returns "gzip".
func (*Config) CompressionName() string {
	return "gzip"
}

// MksquashfsOptions returns options for tuning the compressor.
func (conf *Config) MksquashfsOptions() (opts []string) {
	if val := conf.CompressionLevel; val != 0 {
		opts = append(opts, "-X"+"compression-level", strconv.FormatUint(uint64(val), 10))
	}

	if val := conf.WindowSize; val != 0 {
		opts = append(opts, "-X"+"window-size", strconv.FormatUint(uint64(val), 10))
	}

	if val := conf.Strategy; val != nil {
		s := make([]string, 0, len(val))
		for _, strategy := range val {
			s = append(s, strategy.String())
		}

		opts = append(opts, "-X"+"strategy", strings.Join(s, ","))
	}

	return opts
}

// Strategy identifies a compression strategy specific to gzip.
type Strategy int

const (
	// Default is the default compression strategy.
	Default Strategy = iota
	// Filtered is the "filtered" compression strategy.
	Filtered
	// HuffmanOnly is the "huffman_only" compression strategy.
	HuffmanOnly
	// RunLengthEncoded is the "run_length_encoded" compression strategy.
	RunLengthEncoded
	// Fixed is the "fixed" compression strategy.
	Fixed
)

// String returns the name of the gzip compression strategy.
func (s Strategy) String() string {
	switch s {
	case Default:
		return "default"
	case Filtered:
		return "filtered"
	case HuffmanOnly:
		return "huffman_only"
	case RunLengthEncoded:
		return "run_length_encoded"
	case Fixed:
		return "fixed"
	default:
		panic(fmt.Sprintf("invalid gzip strategy: %d", int(s)))
	}
}
