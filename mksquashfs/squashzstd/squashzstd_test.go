// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashzstd_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashzstd"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashzstd.Config{}
	testutil.Equal(t, cfg.CompressionName(), "zstd")
}

func TestConfig_MksquashfsOptionsDefaults(t *testing.T) {
	cfg := squashzstd.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}

func TestConfig_MksquashfsOptionsWithCompressionLevel(t *testing.T) {
	cfg := squashzstd.Config{CompressionLevel: 9}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "compression-level", "9"})
}
