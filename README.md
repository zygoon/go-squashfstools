<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Go squashfstools module

This module provides as type-safe Go wrapper around the `mksquashfs` and
`unsquashfs` command line utilities. It provides a convenient way of
constructing an `exec.Cmd` instance, ready to be customized or executed to
perform the requested operation.

# Overview of packages

The main packages are `mksquashfs` and `unsquashfs`. Both offer a `Command`
function which takes a number of fixed arguments, options and a variable number
of files. Options are represented as a structure specific to each command. A
convenience `Run` function constructs and executes the command in one go.

The `mksquashfs` package has a number of sub-packages for individual
compression types, for example `mksquashfs/squashgzip`. Configuration of the
compressor is distinct from the rest of compression options, allowing to
support all the complex formats in a concise and uniform way.

In each case the compression parameters are type-safe and are passed as an
interface.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.21 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making
it easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
