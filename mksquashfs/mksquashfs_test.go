// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package mksquashfs_test

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashgzip"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlz4"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlzma"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlzo"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashxz"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashzstd"
)

func TestCommand_WithoutOptions(t *testing.T) {
	cmd := mksquashfs.Command("output.img", nil, "input-file", "input-dir")
	testutil.SlicesEqual(t, cmd.Args, []string{
		"mksquashfs", "input-file", "input-dir", "output.img",
		"-comp", "gzip", "-noappend", "-no-progress", "-no-recovery",
		"-no-exports", "-no-sparse", "-no-xattrs", "-noI", "-noD", "-noF", "-noX",
		"-no-fragments", "-nopad"})
}

func TestCommand_InputFilesAndOutputFiles(t *testing.T) {
	cmd := mksquashfs.Command("output.img", nil, "input-file", "input-dir")
	testutil.SlicesEqual(t, cmd.Args[:4], []string{"mksquashfs", "input-file", "input-dir", "output.img"})
}

var compressConfigs = []mksquashfs.CompressionConfig{
	&squashgzip.Config{},
	&squashlz4.Config{},
	&squashlzma.Config{},
	&squashlzo.Config{},
	&squashxz.Config{},
	&squashzstd.Config{},
}

func TestCommand_CreateWithCompression(t *testing.T) {
	for _, config := range compressConfigs {
		t.Run(config.CompressionName(), func(t *testing.T) {
			cmd := mksquashfs.Command("output.img", &mksquashfs.Options{Compression: config}, "input-dir")
			testutil.WithOptionValue(t, cmd, "-comp", config.CompressionName())
		})
	}
}

func TestCommand_CreateWithBlockSize(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{BlockSize: 4096}, "input-dir")
	testutil.WithOptionValue(t, cmd, "-b", "4096")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{BlockSize: 0}, "input-dir")
	testutil.WithoutOption(t, cmd, "-b")
}

func TestCommand_CreateWithNfsExportable(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{NfsExportable: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-no-exports")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{NfsExportable: false}, "input-dir")
	testutil.WithOption(t, cmd, "-no-exports")
}

func TestCommand_CreateWithSparseFiles(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{Sparse: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-no-sparse")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{Sparse: false}, "input-dir")
	testutil.WithOption(t, cmd, "-no-sparse")
}

func TestCommand_CreateWithXattrs(t *testing.T) {
	// There's an affirmative option for xattrs, unlike most of the other options.
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{Xattrs: true}, "input-dir")
	testutil.WithOption(t, cmd, "-xattrs")
	testutil.WithoutOption(t, cmd, "-no-xattrs")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{Xattrs: false}, "input-dir")
	testutil.WithoutOption(t, cmd, "-xattrs")
	testutil.WithOption(t, cmd, "-no-xattrs")
}

func TestCommand_CreateWithInodeCompression(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{CompressInodes: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-noI")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{CompressInodes: false}, "input-dir")
	testutil.WithOption(t, cmd, "-noI")
}

func TestCommand_CreateWithDataCompression(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{CompressData: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-noD")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{CompressData: false}, "input-dir")
	testutil.WithOption(t, cmd, "-noD")
}

func TestCommand_CreateWithFragmentCompression(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{CompressFragments: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-noF")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{CompressFragments: false}, "input-dir")
	testutil.WithOption(t, cmd, "-noF")
}

func TestCommand_CreateWithXattrCompression(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{CompressXattrs: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-noX")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{CompressXattrs: false}, "input-dir")
	testutil.WithOption(t, cmd, "-noX")
}

func TestCommand_CreateWithFragments(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{Fragments: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-no-fragments")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{Fragments: false}, "input-dir")
	testutil.WithOption(t, cmd, "-no-fragments")
}

func TestCommand_CreateWithPadding(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{Pad: true}, "input-dir")
	testutil.WithoutOption(t, cmd, "-nopad")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{Pad: false}, "input-dir")
	testutil.WithOption(t, cmd, "-nopad")
}

func TestCommand_CreateWithNumProcessors(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{NumProcessors: 2}, "input-dir")
	testutil.WithOptionValue(t, cmd, "-processors", "2")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{NumProcessors: 0}, "input-dir")
	testutil.WithoutOption(t, cmd, "-processors")
}

func TestCommand_CreateWithReadQueueSize(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{ReadQueueSizeMB: 16}, "input-dir")
	testutil.WithOptionValue(t, cmd, "-read-queue", "16")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{ReadQueueSizeMB: 0}, "input-dir")
	testutil.WithoutOption(t, cmd, "-read-queue")
}

func TestCommand_CreateWithWriteQueueSize(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{WriteQueueSizeMB: 16}, "input-dir")
	testutil.WithOptionValue(t, cmd, "-write-queue", "16")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{WriteQueueSizeMB: 0}, "input-dir")
	testutil.WithoutOption(t, cmd, "-write-queue")
}

func TestCommand_CreateWithFragmentQueueSize(t *testing.T) {
	cmd := mksquashfs.Command("output.img", &mksquashfs.Options{FragmentQueueSizeMB: 16}, "input-dir")
	testutil.WithOptionValue(t, cmd, "-frag-queue", "16")

	cmd = mksquashfs.Command("output.img", &mksquashfs.Options{FragmentQueueSizeMB: 0}, "input-dir")
	testutil.WithoutOption(t, cmd, "-frag-queue")
}

// TestReallyMksquashfs checks the interaction of squashfs-tools on the running system.
func TestRun(t *testing.T) {
	testutil.RequiresProgram(t, "mksquashfs")

	d := t.TempDir()

	inDir := filepath.Join(d, "in-dir")
	err := os.Mkdir(inDir, 0o700)
	testutil.Ok(t, err)

	err = os.WriteFile(filepath.Join(inDir, "canary.txt"), []byte("Hello"), 0o600)
	testutil.Ok(t, err)

	imgFile := filepath.Join(d, "image.img")
	opts := mksquashfs.Options{
		Xattrs:        true,
		NfsExportable: true,
		Sparse:        true,

		CompressInodes:    true,
		CompressData:      true,
		CompressXattrs:    true,
		CompressFragments: true,

		Pad:       true,
		Fragments: true,
	}

	for _, config := range compressConfigs {
		t.Run(config.CompressionName(), func(t *testing.T) {
			opts.Compression = config
			err = mksquashfs.Run(imgFile, &opts, inDir)
			testutil.Ok(t, err)

			fi, err := os.Stat(imgFile)
			testutil.Ok(t, err)

			testutil.NotEqual(t, fi.Size(), 0)
		})
	}
}
