// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package mksquashfs provides a Go interface to the mksquashfs(1) program.
package mksquashfs

import (
	"errors"
	"os/exec"
	"strconv"

	"gitlab.com/zygoon/go-squashfstools"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashgzip"
)

// Options describes options for mksquashfs(1).
type Options struct {
	// Compression contains the type and configuration of the compression algorithm.
	Compression CompressionConfig

	// Block size controls a non-standard block size.
	BlockSize uint
	// NfsExportable controls if the file system is compatible with being exported over NFS.
	NfsExportable bool
	// Sparse controls detection of sparse files.
	Sparse bool
	// Xattrs controls storage of extended attributes.
	Xattrs bool
	// CompressInodes controls the compression of inode table.
	CompressInodes bool
	// CompressData controls the compression of data blocks.
	CompressData bool
	// CompressFragments controls the compression of fragment blocks
	CompressFragments bool
	// CompressXattrs controls the compression of extended attributes.
	CompressXattrs bool
	// Fragments controls usage of fragment blocks.
	Fragments bool
	// Pad controls padding the file system to multiples of 4K.
	Pad bool

	// NumProcessors constrains concurrency factor.
	NumProcessors uint
	// ReadQueueSizeMB defines the size of the read queue in megabytes.
	ReadQueueSizeMB uint
	// WriteQueueSizeMB defines the size of the write queue in megabytes.
	WriteQueueSizeMB uint
	// FragmentQueueSizeMB defines the size of the fragment queue in megabytes.
	FragmentQueueSizeMB uint
}

// CompressionConfig describes compression configuration for mksquashfs.
//
// Concrete compression configurations are provided by sub-packages of the
// mksquashfs package.
type CompressionConfig interface {
	CompressionName() string
	MksquashfsOptions() []string
}

const (
	cmdMksquashfs     = "mksquashfs"
	flagCompression   = "-comp"
	flagBlockSize     = "-b"
	flagNoAppend      = "-noappend"
	flagNoFragments   = "-no-fragments"
	flagNoNfsExports  = "-no-exports"
	flagNoPad         = "-nopad" // Actual spelling in mksquashfs.
	flagNoProgress    = "-no-progress"
	flagNoRecovery    = "-no-recovery"
	flagNoSparse      = "-no-sparse"
	flagNoXattrs      = "-no-xattrs"
	flagXattrs        = "-xattrs"
	flagProcessors    = "-processors"
	flagFragmentQueue = "-frag-queue"
	flagReadQueue     = "-read-queue"
	flagWriteQueue    = "-write-queue"
)

// Command returns a configured mksquashfs command.
func Command(toImg string, opts *Options, files ...string) *exec.Cmd {
	if opts == nil {
		opts = &Options{}
	}

	if opts.Compression == nil {
		opts.Compression = &squashgzip.Config{}
	}

	// Sources go first.
	args := make([]string, 0, len(files))
	args = append(args, files...)

	// Destination image goes next.
	args = append(args, toImg)

	// Use prescribed compression algorithm.
	args = append(args, flagCompression, opts.Compression.CompressionName())
	args = append(args, opts.Compression.MksquashfsOptions()...)

	// Several options are fixed.
	args = append(args,
		// Do not append to existing image.
		flagNoAppend,
		// Do not show progress during construction.
		flagNoProgress,
		// Do not create a recovery file.
		flagNoRecovery,
	)

	if opts.BlockSize != 0 {
		args = append(args, flagBlockSize, strconv.FormatUint(uint64(opts.BlockSize), 10))
	}

	if !opts.NfsExportable {
		args = append(args, flagNoNfsExports)
	}

	if !opts.Sparse {
		args = append(args, flagNoSparse)
	}

	if !opts.Xattrs {
		args = append(args, flagNoXattrs)
	} else {
		args = append(args, flagXattrs)
	}

	if !opts.CompressInodes {
		args = append(args, "-noI")
	}

	if !opts.CompressData {
		args = append(args, "-noD")
	}

	if !opts.CompressFragments {
		args = append(args, "-noF")
	}

	if !opts.CompressXattrs {
		args = append(args, "-noX")
	}

	if !opts.Fragments {
		args = append(args, flagNoFragments)
	}

	if !opts.Pad {
		args = append(args, flagNoPad)
	}

	if opts.NumProcessors != 0 {
		args = append(args, flagProcessors, strconv.FormatUint(uint64(opts.NumProcessors), 10))
	}

	if opts.ReadQueueSizeMB != 0 {
		args = append(args, flagReadQueue, strconv.FormatUint(uint64(opts.ReadQueueSizeMB), 10))
	}

	if opts.WriteQueueSizeMB != 0 {
		args = append(args, flagWriteQueue, strconv.FormatUint(uint64(opts.WriteQueueSizeMB), 10))
	}

	if opts.FragmentQueueSizeMB != 0 {
		args = append(args, flagFragmentQueue, strconv.FormatUint(uint64(opts.FragmentQueueSizeMB), 10))
	}

	return exec.Command(cmdMksquashfs, args...)
}

// Run executes mksquashfs(1) with the prescribed configuration.
func Run(toImg string, opts *Options, files ...string) error {
	cmd := Command(toImg, opts, files...)
	_, err := cmd.Output()

	var ee *exec.ExitError

	if errors.As(err, &ee) {
		return squashfstools.NewCommandError(cmd, ee)
	}

	return err
}
