// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashlzma contains LZMA compression configuration for mksquashfs.
package squashlzma

// Config describes configuration of the LZMA compression for mksquashfs(1).
type Config struct{}

// CompressionName returns "lzma".
func (*Config) CompressionName() string {
	return "lzma"
}

// MksquashfsOptions returns options for tuning the compressor.
func (conf *Config) MksquashfsOptions() (opts []string) {
	return nil
}
