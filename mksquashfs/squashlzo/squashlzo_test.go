// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashlzo_test

import (
	"testing"

	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs/squashlzo"
)

func TestConfig_CompressionName(t *testing.T) {
	cfg := squashlzo.Config{}
	testutil.Equal(t, cfg.CompressionName(), "lzo")
}

func TestConfig_MksquashfsOptionsDefaults(t *testing.T) {
	cfg := squashlzo.Config{}
	testutil.SliceHasLen(t, cfg.MksquashfsOptions(), 0)
}

func TestConfig_MksquashfsOptionsWithCompressionLevel(t *testing.T) {
	t.Run("lzo1x999", func(t *testing.T) {
		cfg := squashlzo.Config{
			Algorithm:        squashlzo.Lzo1x999,
			CompressionLevel: 9,
		}

		testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "compression-level", "9"})
	})

	t.Run("lzo1x1", func(t *testing.T) {
		// Compression level only applies to Lzo1x_999.
		cfg := squashlzo.Config{Algorithm: squashlzo.Lzo1x1}
		testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "algorithm", "lzo1x_1"})
	})
}

func TestConfig_MksquashfsOptionsWithAlgorithm(t *testing.T) {
	cfg := squashlzo.Config{Algorithm: squashlzo.Lzo1x1type12}
	testutil.SlicesEqual(t, cfg.MksquashfsOptions(), []string{"-X" + "algorithm", "lzo1x_1_12"})
}

func TestAlgorithm_String(t *testing.T) {
	testutil.Equal(t, squashlzo.Lzo1x1.String(), "lzo1x_1")
	testutil.Equal(t, squashlzo.Lzo1x1type11.String(), "lzo1x_1_11")
	testutil.Equal(t, squashlzo.Lzo1x1type12.String(), "lzo1x_1_12")
	testutil.Equal(t, squashlzo.Lzo1x1type15.String(), "lzo1x_1_15")
	testutil.Equal(t, squashlzo.Lzo1x999.String(), "lzo1x_999")
	testutil.PanicMatches(t, func() { _ = squashlzo.Algorithm(42).String() }, "invalid LZO algorithm: 42")
}
