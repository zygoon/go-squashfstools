// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package squashfstools provides sub-packages wrapping programs from the squashfs-tools suite.
package squashfstools

import (
	"fmt"
	"os/exec"
)

// CommandError is like exec.ExitError but shows exec.Command.Args.
type CommandError struct {
	cmd       *exec.Cmd
	exitError *exec.ExitError
}

// NewCommandError returns a new CommandError with given command and exit error.
func NewCommandError(cmd *exec.Cmd, exitError *exec.ExitError) *CommandError {
	return &CommandError{cmd: cmd, exitError: exitError}
}

// Unwrap returns the error wrapped inside the CommandError.
func (e *CommandError) Unwrap() error {
	return e.exitError
}

// Error implements the error protocol.
func (e *CommandError) Error() string {
	return fmt.Sprintf("command error: %s exited with code %d\n%s",
		e.cmd.Args, e.exitError.ExitCode(), string(e.exitError.Stderr))
}
