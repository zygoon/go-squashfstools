// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package squashfstools_test

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"gitlab.com/zygoon/go-squashfstools"
	"gitlab.com/zygoon/go-squashfstools/internal/testutil"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs"
	"gitlab.com/zygoon/go-squashfstools/unsquashfs"
)

func TestPackUnpack(t *testing.T) {
	testutil.RequiresProgram(t, "mksquashfs", "unsquashfs")

	d := t.TempDir()

	inDir := filepath.Join(d, "in-dir")
	err := os.Mkdir(inDir, 0o700)
	testutil.Ok(t, err)

	err = os.WriteFile(filepath.Join(inDir, "canary.txt"), []byte("Hello"), 0o600)
	testutil.Ok(t, err)

	imgFile := filepath.Join(d, "image.img")
	err = mksquashfs.Run(imgFile, nil, inDir)
	testutil.Ok(t, err)

	_, err = os.Stat(imgFile)
	testutil.Ok(t, err)

	outDir := filepath.Join(d, "out-dir")
	err = unsquashfs.Run(imgFile, outDir, nil, "canary.txt")
	testutil.Ok(t, err)

	data, err := os.ReadFile(filepath.Join(outDir, "canary.txt"))
	testutil.Ok(t, err)
	testutil.SlicesEqual(t, data, []byte("Hello"))
}

func TestNewCommandError_Error(t *testing.T) {
	testutil.RequiresProgram(t, "false")

	cmd := exec.Command("false")
	_, err := cmd.Output()
	e := squashfstools.NewCommandError(cmd, err.(*exec.ExitError))

	testutil.ErrorMatches(t, e, `command error: \[false\] exited with code 1`+"\n")
	testutil.Equal(t, errors.Unwrap(e), err)

	cmd = exec.Command("sh", "-c", "echo boom >&2 && exit 2")
	_, err = cmd.Output()
	e = squashfstools.NewCommandError(cmd, err.(*exec.ExitError))

	testutil.ErrorMatches(t, e, `command error: \[sh -c echo boom >&2 && exit 2\] exited with code 2`+"\n"+`boom`+"\n")
	testutil.Equal(t, errors.Unwrap(e), err)
}
